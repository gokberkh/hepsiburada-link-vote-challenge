import React from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import userEvent from "@testing-library/user-event";
import AddLink from "../pages/AddLink";
import App from "../App";
import { BrowserRouter as Router } from "react-router-dom";

const mockedUsedNavigate = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockedUsedNavigate,
}));

test("form placeholders correctly", () => {
  const { getByPlaceholderText, debug } = render(<AddLink />);
  expect(getByPlaceholderText("e.g. Alphabet")).toBeInTheDocument();
  expect(getByPlaceholderText("e.g. http://abc.xyz")).toBeInTheDocument();
});

test("form type correctly", () => {
  const { getByPlaceholderText, getByRole } = render(<AddLink />);
  const addButton = getByRole("button", { name: /add/i });
  const linkName = getByPlaceholderText("e.g. Alphabet");
  const linkURL = getByPlaceholderText("e.g. http://abc.xyz");
  expect(addButton.type).toEqual("submit");
  expect(linkName.type).toEqual("text");
  expect(linkURL.type).toEqual("text");
});

test("button click and required correctly", () => {
  const { getByRole, screen, findByText } = render(<AddLink />);
  const addButton = getByRole("button", { name: /add/i });
  userEvent.click(addButton);
});

test("submit works properly", () => {
  const mockFormData = {
    linkName: "test name",
    url: "test url",
  };
  const { getByPlaceholderText, getByRole, getByText } = render(<AddLink />);

  userEvent.paste(getByPlaceholderText("e.g. Alphabet"), mockFormData.linkName);
  userEvent.paste(
    getByPlaceholderText("e.g. http://abc.xyz"),
    mockFormData.url
  );
  const addButton = getByRole("button", { name: /add/i });
  userEvent.click(addButton);
});

test("return button properly", () => {
  const { getByPlaceholderText, getByRole, getByText } = render(<AddLink />);
  const addButton = getByRole("button", { name: /return/i });
  userEvent.click(addButton);
});
