import Pagination from "../components/Pagination";
import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { mount } from "enzyme";

describe("<Pagination/>", () => {
  it("should render correctly", () => {
    let testingData = [
      {
        id: 416807,
        title: "123",
        url: "44",
        points: 0,
        createdAt: 1647725761112,
      },
      {
        id: 178610,
        title: "asd",
        url: "asd",
        points: 0,
        createdAt: 1647725759051,
      },
      {
        id: 750777,
        title: "123",
        url: "231",
        points: 0,
        createdAt: 1647725757374,
      },
      {
        id: 122811,
        title: "asd",
        url: "das",
        points: 0,
        createdAt: 1647725755613,
      },
      {
        id: 340971,
        title: "asdasd",
        url: "213",
        points: 0,
        createdAt: 1647717419619,
      },
      {
        id: 4,
        title: "4TEST",
        url: "https://www.3test.com",
        points: 20,
        createdAt: 1637642715923,
      },
    ];
    const pagination = (
      <Pagination toBePaginate={testingData} onChange={() => 2} />
    );
    const component = mount(<div>{pagination}</div>);
    const ul = component.find("ul");
    expect(ul.length).toBe(1);
    expect(ul.hasClass("pagination")).toBe(true);
  });
});
