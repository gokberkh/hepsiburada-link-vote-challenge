import React from "react";
import { Route } from "react-router-dom";
import App from "../App";
import { shallow } from "enzyme";

test("should render pages correctly", () => {
  const app = shallow(<App />);
  const routes = app.find(Route);
  expect(routes.length).toBe(3);
  expect(routes.get(1).props.path).toBe("/");
  expect(routes.get(0).props.path).toBe("/add-link");
});
