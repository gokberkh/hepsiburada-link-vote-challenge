import React, { useState } from "react";
import { AddItem } from "../services/service";
import Alert from "../components/Alert";
import "../styles/AddLink.css";
import "../styles/App.css";
import ReturnButton from "../objects/ReturnButton";

function AddLink() {
  const [linkName, setLinkName] = useState("");
  const [url, setUrl] = useState("");

  function onSubmit(e) {
    e.preventDefault();

    let object = {
      id: Math.floor(Math.random() * 1000000),
      title: linkName,
      url: url,
      points: 0,
      createdAt: new Date().getTime(),
    };

    AddItem(object);
    Alert(linkName.toUpperCase() + " Added.");
    setLinkName("");
    setUrl("");
  }

  return (
    <>
      <div className="addLinkContainer">
        <ReturnButton />
      </div>

      <h1 style={{ marginTop: "40px", fontWeight: "bold", fontSize: "30px" }}>
        Add New Link
      </h1>

      <form onSubmit={onSubmit}>
        <div className="form-group">
          <label htmlFor="linkNameInput">Link Name:</label>
          <input
            type="text"
            className="form-control"
            id="linkNameInput"
            placeholder="e.g. Alphabet"
            value={linkName}
            onChange={(e) => setLinkName(e.target.value)}
            required
          />
        </div>
        <div className="mt-4 form-group">
          <label htmlFor="linkUrlInput">Link URL:</label>
          <input
            type="text"
            className="form-control"
            id="linkUrlInput"
            placeholder="e.g. http://abc.xyz"
            value={url}
            onChange={(e) => setUrl(e.target.value)}
            required
          />
        </div>

        <div className="d-flex justify-content-end">
          <button type="submit" className="mt-5 subButton">
            ADD
          </button>
        </div>
      </form>
    </>
  );
}

export default AddLink;
