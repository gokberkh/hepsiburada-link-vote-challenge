import React from "react";
import NotFoundImage from "../assets/404.jpg";

export default function NotFound() {
  return (
    <div>
      <img src={NotFoundImage} width="100%" height="50%" alt="notfound" />
    </div>
  );
}
