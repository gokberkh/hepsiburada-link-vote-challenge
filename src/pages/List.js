import React, { useEffect, useState, memo } from "react";
import CardComp from "../components/CardComp";
import Select from "react-select";
import Pagination from "../components/Pagination.js";
import SubmitButton from "../objects/SubmitButton";
import { SortByData } from "../services/service";

function List() {
  const [selectedOption, setSelectedOption] = useState(null);
  const [linkData, setLinkData] = useState("");

  const options = [
    { value: "mostSort", label: "Most Voted (Z➟A)" },
    { value: "lessSort", label: "Less Voted (A➟Z)" },
  ];

  function updateData(data) {
    setLinkData(data);
  }

  useEffect(() => {
    setLinkData(JSON.parse(localStorage.getItem("linkData")));
  }, []);

  const handleChange = (option) => {
    const type = option.value;
    setSelectedOption(type);
    setLinkData(SortByData(linkData, type));
  };

  return (
    <div>
      <SubmitButton />
      <div style={{ width: "50%" }}>
        <Select
          options={options}
          defaultValue={selectedOption}
          onChange={handleChange}
          placeholder="Order by"
        />
      </div>
      <br />
      {linkData.length > 5 ? (
        <Pagination toBePaginate={linkData} getData={updateData} />
      ) : (
        <CardComp dataToBeMapped={linkData} getData={updateData} />
      )}
    </div>
  );
}

export default memo(List);
