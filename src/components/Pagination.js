import React, { useState, useEffect } from "react";
import ReactPaginate from "react-paginate";
import { MdArrowBackIos, MdArrowForwardIos } from "react-icons/md";
import "../styles/Pagination.css";
import CardComp from "./CardComp";

function Pagination({ toBePaginate, getData }) {
  const [postsPerPage] = useState(5);
  const [offset, setOffset] = useState(1);
  const [posts, setAllPosts] = useState([]);
  const [pageCount, setPageCount] = useState(0);

  const getPostData = (data) => {
    return <CardComp dataToBeMapped={data} getData={getData} />;
  };

  const getAllPosts = async (toBePaginate) => {
    const data = JSON.parse(localStorage.getItem("linkData"));

    const indexOfLastPost = offset * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;

    const slice = toBePaginate.slice(indexOfFirstPost, indexOfLastPost);
    const postData = getPostData(slice);

    setAllPosts(postData);
    setPageCount(Math.ceil(toBePaginate.length / postsPerPage));
  };

  const handlePageClick = (event) => {
    const selectedPage = event.selected;
    setOffset(selectedPage + 1);
  };

  useEffect(() => {
    getAllPosts(toBePaginate);
  }, [offset, toBePaginate]);

  return (
    <>
      {posts}
      <ReactPaginate
        previousLabel={<MdArrowBackIos color="black" />}
        nextLabel={<MdArrowForwardIos color="black" />}
        breakLabel={"..."}
        breakClassName={"break-me"}
        pageCount={pageCount}
        onPageChange={handlePageClick}
        containerClassName={"pagination"}
        subContainerClassName={"pages pagination"}
        activeClassName={"active"}
      />
    </>
  );
}

export default Pagination;
