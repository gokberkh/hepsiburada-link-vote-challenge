import React, { useState } from "react";
import { Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import { MdThumbUp } from "react-icons/md";
import { MdThumbDown } from "react-icons/md";
import deleteButton from "../assets/delete.png";
import { VoteItem } from "../services/service";
import ModalComp from "./ModalComp";
import "../styles/CardComp.css";

function CardComp({ dataToBeMapped, getData }) {
  const [show, setShow] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);
  const handleClose = () => setShow(false);

  return (
    <>
      {dataToBeMapped &&
        dataToBeMapped.map((item) => (
          <Card key={item.id} className="cardContainer">
            <Card.Body className="cardBody">
              <Row>
                <Col className="pointContainer" xs={3} md={3}>
                  <span className="fw-bold" style={{ fontSize: "40px" }}>
                    {item.points}
                  </span>
                  <span className="fw-normal" style={{ fontSize: "15px" }}>
                    POINTS
                  </span>
                </Col>
                <Col xs={6} md={6} className="cardContent">
                  <div>
                    <span className="fw-bold">{item.title}</span>
                    <small className="text-muted">{item.url}</small>
                  </div>
                  <div
                    className="d-flex justify-content-between mt-2 "
                    style={{ fontSize: "18px" }}
                  >
                    <button
                      className="vote"
                      onClick={() => {
                        getData(VoteItem(item, "up"));
                      }}
                    >
                      <MdThumbUp color="green" />
                      <small className="text-muted">Up Vote</small>
                    </button>
                    <button
                      className="vote"
                      onClick={() => getData(VoteItem(item, "down"))}
                    >
                      <MdThumbDown color="red" />
                      <small className="text-muted">Down Vote</small>
                    </button>
                  </div>
                </Col>
              </Row>
            </Card.Body>
            <button
              className="delete"
              onClick={() => {
                setShow(true);
                setSelectedItem(item);
              }}
            >
              <img src={deleteButton} alt="delete" />
            </button>
          </Card>
        ))}

      {selectedItem && selectedItem !== null ? (
        <ModalComp
          selectedItem={selectedItem}
          isOpen={show}
          handleClose={handleClose}
          getData={getData}
        />
      ) : (
        ""
      )}
    </>
  );
}

export default CardComp;
