import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Alert(content) {
  return toast.success(content, {
    position: "top-center",
    autoClose: 2500,
    closeOnClick: false,
    pauseOnHover: true,
    closeButton: false,
    draggable: true,
    progress: false,
    theme: "colored",
    hideProgressBar: true,
  });
}

export default Alert;
