import React from "react";
import hbLogo from "../assets/hepsiburadalogo.svg";

export const Header = () => {
  return (
    <div className="row border-bottom ">
      <div className="col">
        <div className="row">
          <div className="col d-flex justify-content-center justify-content-md-start">
            <a
              className="logo"
              href="https://www.hepsiburada.com/"
              title="Hepsiburada"
              style={{
                backgroundImage: `url(${hbLogo})`,
              }}
            />
          </div>
          <div className="col d-flex justify-content-md-end  justify-content-center">
            <span className="fw-bold">Link</span>
            <span className="fw-lighter">VOTE &nbsp;</span>
            <span>Challenge</span>
          </div>
        </div>
      </div>
    </div>
  );
};
