import React from "react";
import "../styles/SubmitButton.css";
import { FiPlus } from "react-icons/fi";
import { useNavigate } from "react-router-dom";

const SubmitButton = () => {
  let navigate = useNavigate();
  const routeChange = () => {
    navigate(`/add-link`);
  };
  return (
    <div>
      <button className="d-flex p-2 mb-3 highlightBg" onClick={routeChange}>
        <div className="submitIcon">
          <FiPlus size={88} />
        </div>
        <span className="fw-bold mx-4 submitName">SUBMIT A LINK</span>
      </button>
    </div>
  );
};

export default SubmitButton;
