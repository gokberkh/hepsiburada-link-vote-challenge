import React from "react";
import { useNavigate } from "react-router-dom";
import { HiArrowLeft } from "react-icons/hi";
import "../styles/ReturnButton.css";

const ReturnButton = () => {
  let navigate = useNavigate();
  return (
    <button onClick={() => navigate(-1)} className="returnButton">
      <span>
        <HiArrowLeft size={20} />
        Return to List
      </span>
    </button>
  );
};
export default ReturnButton;
